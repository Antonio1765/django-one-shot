from django.urls import path
from todos.views import todo_show_list

urlpatterns = [
    path("", todo_show_list, name="todo_show_list"),
]
