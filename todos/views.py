from django.shortcuts import render, get_object_or_404, redirect
from django.shortcuts import render
from todos.models import TodoList

# Create your views here.
def todo_show_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_show_list": todos,
    }
    return render(request, "todos/list.html", context)
